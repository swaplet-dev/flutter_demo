import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';


class MapIconButtons extends StatelessWidget {
  const MapIconButtons({
    Key key,
    this.goToMyLocation, 
    this.setMarker, 
    this.btnTitle,
    this.btnIcon
  });
  final Function goToMyLocation;
  final Function setMarker;
  final String btnTitle;
  final IconData btnIcon;


  @override
  Widget build(BuildContext context) {
    return Row( children: <Widget>[
      
        Column( children: <Widget>[
             FloatingActionButton(onPressed: goToMyLocation, 
                              child: Icon(Icons.my_location),
                              backgroundColor: Colors.green[300],),
             
              Text('go to my location')
        ], mainAxisAlignment: MainAxisAlignment.center,),
        

        Column(children: <Widget>[
        FloatingActionButton(onPressed: setMarker, 
                              child: Icon(btnIcon),
                              backgroundColor: Colors.purple[300],),
          Text(btnTitle)
          ], mainAxisAlignment: MainAxisAlignment.center,),
      ],mainAxisAlignment: MainAxisAlignment.spaceEvenly,);

  }
}


class MapScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MapWidget();
  }
}

class MapWidget extends StatefulWidget {
  @override
  _MapWidgetState createState() => _MapWidgetState();
}

class _MapWidgetState extends State<MapWidget> {
  GoogleMapController mapController;
  List<Marker> allMarkers = [];
  final List<String> optionsMarkerBtnTitle = ['set marker', 'remove marker'];
  final List<IconData> optionsMarkerBtnIcon = [Icons.add_location, Icons.location_off];
  String markerBtnTitle = 'set marker';
  IconData markerbtnIcon = Icons.add_location;
  int markerIndex = 0;

  LatLng _center = const LatLng(45.521563, -122.677433);

  void _onMapCreated (GoogleMapController controller)  {
    mapController = controller;
  }

  void myLocation() async {
    this.setState(() async {
      Position pos   = await Geolocator().getLastKnownPosition(desiredAccuracy: LocationAccuracy.high);
      this._center = LatLng(pos.latitude, pos.altitude);
      mapController.animateCamera(CameraUpdate.newLatLng(_center));
      this.allMarkers.add(Marker(
      markerId: MarkerId('home'), 
      position: _center,));
      // this.mapController.moveCamera();      
    });
  }


  void _setMarker() {
    this.setState(() {
      
      // markerIndex = (markerIndex > 0) ? 0 : 1;
      if (markerIndex > 0) {
         markerIndex = 0;
         for (int i=0; i<allMarkers.length; i++)
         {
           if (allMarkers[i].markerId == MarkerId('place1'))
           {
             allMarkers.removeAt(i);
           }
         }
      } 
      else {
        markerIndex = 1;
        LatLng pos = LatLng(_center.latitude+0.3, _center.longitude+0.8);
        this.allMarkers.add(Marker(markerId: MarkerId('place1'), position: pos));
        }
      
      this.markerBtnTitle = this.optionsMarkerBtnTitle[markerIndex];
      this.markerbtnIcon = this.optionsMarkerBtnIcon[markerIndex];
    });

    @override
    void initState(){
      super.initState();
      this.markerBtnTitle = this.optionsMarkerBtnTitle[markerIndex];
      this.markerbtnIcon = this.optionsMarkerBtnIcon[markerIndex];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('fuck JS'),
          backgroundColor: Colors.blueAccent,
        ),
        body:
        Column( children: <Widget>[
          Expanded(
            child: MapIconButtons(goToMyLocation: myLocation, setMarker: _setMarker,
                                  btnIcon: markerbtnIcon,
                                  btnTitle: markerBtnTitle,),
            flex: 2),          
          // Expanded( child: Divider(color: Colors.black,), flex: 1,),
          Expanded (
          child: GoogleMap(
            onMapCreated: _onMapCreated,
            initialCameraPosition: CameraPosition(
              target: _center,
              zoom: 7.0,              
            ),
          markers: Set.from(allMarkers),),
          flex : 7,
        ),
          ]),
    );
  }
}